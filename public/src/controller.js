angular.module('WeightTrackerApp')
	.controller('ListController', function ($scope,$rootScope,Record,$location) {
        $rootScope.PAGE = "all";

        $scope.records = Record.query();
        $scope.fields = ['date','weight','note'];

        $scope.sort = function (field) {
            $scope.sort.field = field;
            $scope.sort.order = !$scope.sort.order;
        };

        $scope.sort.field = 'date';
        $scope.sort.order = false;

        $scope.show = function (id) {
            $location.url('/record/' + id);
        };

	})
	.controller('NewController', function ($scope,$rootScope,Record,$location) {
    	$rootScope.PAGE = "new";
        $scope.record = new Record({
            date:       ['', 'text'],
            weight:     ['', 'number'],
            note:       ['', 'text']
        });

        $scope.save = function () {
            $scope.record.$save();
            $location.url('/records');
        };
	})
    .controller('EditController', function ($scope,$rootScope,Record,$location,$routeParams) {
        $rootScope.PAGE = "edit";
        $scope.record = Record.get({ id: parseInt($routeParams.id, 10) }); 
        $scope.delete = function () {
            $scope.record.$delete();
            $location.url('/records');
        };
        $scope.save = function () {
            $scope.record.$update(function (updatedRecord){
            });
            $location.url('/records');
        };
    });