angular.module('WeightTrackerApp',['ngRoute','ngResource'])
	.config(function ($routeProvider, $locationProvider) {
        $routeProvider
            .when('/records', {
            	controller: 'ListController',
                templateUrl: 'views/list.html'
            })
            .when('/record/new', {
            	controller: 'NewController',
                templateUrl: 'views/new.html'
            })
            .when('/record/:id', {
                controller: 'EditController',
                templateUrl: 'views/edit.html'
            })
            .otherwise({
                redirectTo: '/records'   
            });
            $locationProvider.html5Mode(true);
    });