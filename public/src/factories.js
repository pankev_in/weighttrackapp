angular.module('WeightTrackerApp')
    .factory('Record', function ($resource) {
        return $resource('/api/record/:id', { id: '@id' }, {
            'update': { method: 'PUT' }
        });
    });