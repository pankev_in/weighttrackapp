var express		= require('express'),
	api 		= require('./api'),
	users   	= require('./accounts'),
	app 		= express();

// Set base directory of static files:
app.use(express.static('./public'));
app.use(users);
app.use('/api', api);

// Set GET route:
app.get('*', function (req, res) {
    if (!req.user) {
        res.redirect('/login');
    } else {
        res.sendfile('public/records.html', {"root": "."});
    }
});

// Start listen on port 3000
app.listen(3000);